

function DateThai(input) {
  const monthNamesThai = ["", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤษจิกายน", "ธันวาคม"];
  const dayNames = ["วันอาทิตย์ที่", "วันจันทร์ที่", "วันอังคารที่", "วันพุทธที่", "วันพฤหัสบดีที่", "วันศุกร์ที่", "วันเสาร์ที่"];
  const mountCode = ["",0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5]

  let date = input.split('/')
  let day = parseInt(date[0], 10)
  let month = parseInt(date[1], 10)
  let year = parseInt(date[2])
  let shorttyYear = parseInt(date[2].substring(2))
  let ceturyCode

  if ( year >= 2300 )
    ceturyCode = 0
  else if ( year >= 2200 )
    ceturyCode = 2
  else if ( year >= 2100 )
    ceturyCode = 4
  else if ( year >= 2000 )
    ceturyCode = 6
  else if ( year >= 1900 )
    ceturyCode = 0
  else if ( year >= 1800 )
    ceturyCode = 2
  else if ( year >= 1700 )
    ceturyCode = 4

  let yearCode = (shorttyYear+(Math.trunc(shorttyYear / 4)))%7

  let leapYear = month <= 2 ? checkLeapYear(year) : 0

  let weekDay = (yearCode+mountCode[month]+ceturyCode+day-leapYear)%7

  return dayNames[weekDay]+" "+day+" "+monthNamesThai[month]+" "+year
}

function checkLeapYear(year) {
  if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
      return 1
  } else {
      return 0
  }
}

let input = "10/10/1996"
console.log(DateThai(input))

